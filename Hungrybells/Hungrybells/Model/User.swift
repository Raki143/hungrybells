//
//  User.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 20/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation

struct User {
    let name : String
    let phoneNumber : String
    
    init(name : String,phoneNumber : String){
        self.name = name
        self.phoneNumber = phoneNumber
    }
    
    init(dictionary : [String : Any]) {
        self.name = dictionary[UserKeys.contactName] as? String ?? ""
        self.phoneNumber = dictionary[UserKeys.contactNumber] as? String ?? ""
    }
}
