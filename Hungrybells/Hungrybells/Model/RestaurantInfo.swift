//
//  RestaurantInfo.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 20/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation

struct RestaurantInfo {
    
    let totalTables : Int
    let occupiedTables : Int
    let countInWaitingQueue : Int
    
    init(dictionary : [String : Any]) {
        totalTables = dictionary[RestaurantKeys.totalTables] as? Int ?? 0
        occupiedTables = dictionary[RestaurantKeys.occupiedTables] as? Int ?? 0
        countInWaitingQueue = dictionary[RestaurantKeys.countInWaitingQueue] as? Int ?? 0
    }
}
