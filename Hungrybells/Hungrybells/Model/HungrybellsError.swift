//
//  HungrybellsError.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 21/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation

enum HungrybellsError : Error{
    case errorInFetchRestaurantInfo
    case errorInUpdateRestaurantInfo
    case errorInAllocating
    case errorInAddingToWaitingQueue
}
