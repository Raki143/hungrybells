//
//  Constants.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 21/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation

struct FirebaseDB {
    static let restaurantInfo = "RestaurantInfo"
    static let tables = "Tables"
    static let waitingQueue = "WaitingQueue"
}

struct RestaurantKeys {
    static let totalTables = "totalTables"
    static let occupiedTables = "occupiedTables"
    static let countInWaitingQueue = "countInWaitingQueue"
}

struct UserKeys {
    static let contactName = "contactName"
    static let contactNumber = "contactNumber"
}

struct AlertMsgs{
    static let highDemand = "Due to high demand. User request is added to waiting queue."
    static let inConvenience = "Sorry for the inconvience, Unable to serve the request. Please try again later."
}
