//
//  Alert.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 21/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation
import UIKit

struct Alert{
    
    // show Alert messages
    static func showAlert(alertMessge:String, controller: UIViewController){
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert" , message: alertMessge, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            controller.present(alert, animated: true, completion: nil)
            
        }
    }
    
}
