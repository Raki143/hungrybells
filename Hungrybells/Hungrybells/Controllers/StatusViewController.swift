//
//  StatusViewController.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 20/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
import Firebase

class StatusViewController: UIViewController {

    
    @IBOutlet weak var contactNumber: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    var reference : DatabaseReference?
    var usersInQueue = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Customer Status"
        searchButton.layer.cornerRadius = 5.0
        searchButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        
        contactNumber.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
         reference = Database.database().reference()
        
        reference?.child("WaitingQueue").observe(DataEventType.value, with: { (snapshot) in
            
            self.usersInQueue = []
            
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    if let userDict = snap.value as? [String : AnyObject]{
                        let user = User(dictionary: userDict)
                        self.usersInQueue.append(user)
                    }
                }
            }
        })
    }

    
    @objc func handleTextInputChange() {
        let isFormValid = contactNumber.text?.count ?? 0 > 0
        
        if isFormValid {
            searchButton.isEnabled = true
            searchButton.backgroundColor = UIColor.white
        } else {
            searchButton.isEnabled = false
            searchButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        }
    }
    
    @IBAction func searchStatus(_ sender: Any) {
        
        UIApplication.shared.keyWindow?.endEditing(true)
        
        if let searchContactNumber = contactNumber.text{
          
           let (result,value) = findUserInQueue(contactNumber: searchContactNumber)
            
            if result == true {
                
                // User available in waiting queue
                Alert.showAlert(alertMessge: "Customer available at \(value!) position in waiting queue", controller: self)
                
            }else{
                // User not available in waiting queue
                Alert.showAlert(alertMessge: "Customer not available in waiting queue", controller: self)
            }
        }
       
        
    }
    
    private func findUserInQueue(contactNumber : String) -> (Bool,Int?) {
        
        for i in usersInQueue.enumerated(){
            if contactNumber == i.element.phoneNumber{
                return (true,i.offset + 1)
            }
        }
        
        return (false,nil)
    }

}
