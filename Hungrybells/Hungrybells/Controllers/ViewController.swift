//
//  ViewController.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 20/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    var restauranrInfo : RestaurantInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add sample restaurant Information to firbase
  
        let infoDictionary = [RestaurantKeys.totalTables:6,RestaurantKeys.occupiedTables:0,RestaurantKeys.countInWaitingQueue:0]
        
        Database.database().reference().child(FirebaseDB.restaurantInfo).updateChildValues(infoDictionary) { (error, ref) in
            
            if let err = error {
                print("Failed to save RestaurantInfo into db:", err)
                return
            }
            
            print("successfully saved RestaurantInfo to db")
            
        }
 
        
        
        
    }
    
}

