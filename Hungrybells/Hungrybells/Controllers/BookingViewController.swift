//
//  BookingViewController.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 20/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
import Firebase

typealias GetStatusCompletionHandler = (_ error:HungrybellsError?,_ status:Bool) -> Void

class BookingViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    var spinnerActivity : MBProgressHUD?
    var reference : DatabaseReference?
    var restaurant : RestaurantInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationItem.title = "Book A Table"
        nameTextField.delegate = self
        contactTextField.delegate = self
        
        submitButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        nameTextField.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        contactTextField.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        reference = Database.database().reference()
        
    }

    @objc func handleTextInputChange() {
        let isFormValid = nameTextField.text?.count ?? 0 > 0 && contactTextField.text?.count ?? 0 > 0
        
        if isFormValid {
            submitButton.isEnabled = true
            submitButton.backgroundColor = UIColor.white
        } else {
            submitButton.isEnabled = false
            submitButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        }
    }
    
    // MARK: - Methods to read,write and update information on Firebase
    private func fetchRestaurantInfo(withCompletion:@escaping (_ info:[String : Any ]) -> Void){
        
        reference?.child(FirebaseDB.restaurantInfo).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            
            
            guard let dictionary = snapshot.value as? [String : Any] else{
                self.stopActivityIndicator()
                Alert.showAlert(alertMessge: AlertMsgs.inConvenience, controller: self)
                return
            }
            
            print("restaurant info from firebase db:",dictionary)
            
            withCompletion(dictionary)
            
            
        }) { (error) in
            
            print("Failed to fetch restaurant info details",error)
            self.stopActivityIndicator()
            Alert.showAlert(alertMessge: AlertMsgs.inConvenience, controller: self)
            return
            
        }
    }
    
    private func updateRestaurantInfo(info:[String:Any],completionHandler : @escaping GetStatusCompletionHandler ){
        
        reference?.child(FirebaseDB.restaurantInfo).updateChildValues(info) { (error, ref) in
            
            if let err = error {
                print("Failed to save RestaurantInfo into db:", err)
                completionHandler(.errorInUpdateRestaurantInfo, false)
                return
            }
            
            print("successfully saved RestaurantInfo to db")
            completionHandler(nil, true)
        }
    }
    
   private func allocateTableToUser(userInfo:[String:String],completionHandler : @escaping GetStatusCompletionHandler ) {
        
        let key = reference?.child(FirebaseDB.tables).childByAutoId().key
        
     
        let childUpdates = ["/\(FirebaseDB.tables)/\(key!)": userInfo]
        
        reference?.updateChildValues(childUpdates) { (error, ref) in
        
            if let err = error {
                print("Failed to save userInfoDictionary into Tables child in db:", err)
                completionHandler(.errorInAllocating, false)
                return
            }
            
            print("successfully allocated table to user and saved userInfoDictionary into Tables child in db")
            
            // update occupied tables data in firebase
            guard let occupiedTables = self.restaurant?.occupiedTables ,let totalTables = self.restaurant?.totalTables else {
                completionHandler(.errorInFetchRestaurantInfo, false)
                return
            }
            
            let updatedOccupiedTables = occupiedTables + 1
            
            let infoDictionary = [RestaurantKeys.totalTables:totalTables, RestaurantKeys.occupiedTables : updatedOccupiedTables]
            
            self.updateRestaurantInfo(info: infoDictionary, completionHandler: { (err, status) in
                completionHandler(err, status)
            })
            
        }
        
        
    }
    
  private func addUserToWaitingQueue(userInfo:[String:String],completionHandler : @escaping GetStatusCompletionHandler ){
        
        let key = reference?.child(FirebaseDB.waitingQueue).childByAutoId().key
        
        
        let childUpdates = ["/\(FirebaseDB.waitingQueue)/\(key!)": userInfo]
        
        reference?.updateChildValues(childUpdates) { (error, ref) in
            
            if let err = error {
                print("Failed to save userInfo into WaitingQueue child in db:", err)
                completionHandler(.errorInAddingToWaitingQueue, false)
                return
            }
            
            print("successfully added user to WaitingQueue in Firebase.")
            
            // update countInWaitingQueue in restaurantInfo
            guard let occupiedTables = self.restaurant?.occupiedTables ,let totalTables = self.restaurant?.totalTables,let count = self.restaurant?.countInWaitingQueue else {
                completionHandler(.errorInFetchRestaurantInfo, false)
                return
            }
            
            let updatedCountInQueue = count + 1
            
            let infoDictionary = [RestaurantKeys.totalTables : totalTables,RestaurantKeys.occupiedTables : occupiedTables, RestaurantKeys.countInWaitingQueue : updatedCountInQueue]
            
            self.updateRestaurantInfo(info: infoDictionary, completionHandler: { (err, status) in
                completionHandler(err,status)
            })
            
        }
        
    }
    
    @IBAction func serveUserRequest(_ sender: Any) {
        
        UIApplication.shared.keyWindow?.endEditing(true)
        
        // start Activity Indicator
        self.startActivityIndicator(view: self.view)
        
        guard let contactName = nameTextField.text, let contactNumber = contactTextField.text else {
            self.stopActivityIndicator()
            return
        }
        
        let userInfoDictionary = [UserKeys.contactName : contactName, UserKeys.contactNumber : contactNumber]
        
        // fetch RestaurantInfo
        fetchRestaurantInfo { (dictionary) in
            
             self.restaurant = RestaurantInfo(dictionary: dictionary)
            
            // check vacant table is available
            guard let occupiedTables = self.restaurant?.occupiedTables ,let totalTables = self.restaurant?.totalTables else {
                self.stopActivityIndicator()
                return
            }
            
            if(occupiedTables < totalTables){
                
                // allocate vacant table to customer
                self.allocateTableToUser(userInfo: userInfoDictionary, completionHandler: { (err,status) in
                    
                    self.stopActivityIndicator()
                    
                    if err != nil {
                       // error had occured
                        Alert.showAlert(alertMessge: AlertMsgs.inConvenience, controller: self)
                    }else{
                        
                        // successfully served the request
                        Alert.showAlert(alertMessge: "Table is allocated to the \(contactName)", controller: self)
                        
                         self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                })
                
            }else{
                
                // add user to waiting queue
                print("add user to waiting queue.")
                
                self.addUserToWaitingQueue(userInfo: userInfoDictionary, completionHandler: { (err, status) in
                    
                    self.stopActivityIndicator()
                    
                    if err != nil {
                        // error had occured
                        Alert.showAlert(alertMessge: AlertMsgs.inConvenience, controller: self)
                    }else{
                        
                        // successfully added to the waiting queue
                        Alert.showAlert(alertMessge: AlertMsgs.highDemand, controller: self)
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                })
                
            }
            
        }
        
        
    }
    
   // MARK: - Activity Indicator
    
    // show Activity Indicator
    func startActivityIndicator(view:UIView){
        
        DispatchQueue.main.async {
            self.spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
            self.spinnerActivity?.label.text = "Loading";
            self.spinnerActivity?.isUserInteractionEnabled = false;
        }
        
        
    }
    
    
    // Hide Activity Indicator
    func stopActivityIndicator(){
        DispatchQueue.main.async {
            self.spinnerActivity?.hide(animated: true)
        }
    }
    
}

 // MARK: - UITextFieldDelegate
extension BookingViewController : UITextFieldDelegate {
    // MARK: - KeyBoard Resigning and Notification
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
