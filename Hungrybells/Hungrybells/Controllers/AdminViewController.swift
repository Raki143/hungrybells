//
//  AdminViewController.swift
//  Hungrybells
//
//  Created by Rakesh Kumar on 20/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit

class AdminViewController: UIViewController {

    @IBOutlet weak var bookingButton: UIButton!
    @IBOutlet weak var statusButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       self.navigationItem.title = "Menu"
        bookingButton.layer.cornerRadius = 5.0
        statusButton.layer.cornerRadius = 5.0
        
    }

    @IBAction func bookTable(_ sender: Any) {
        // Move to Booking ViewController
        performSegue(withIdentifier:"BookingVC", sender: nil)
    }
    
    
    @IBAction func checkUserStatus(_ sender: Any) {
        // Move to Status ViewController
        performSegue(withIdentifier: "StatusVC", sender: nil)
    }
}
