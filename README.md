# HungryBells

HungryBells is a table booking app in Swift.  Following are the key features the app should have:

1. As a receptionist, I should be able to enter a customer in the waiting queue. (Example: Enter name and contact number)
2. As a receptionist, I should be able to allocate a table to the first person in the queue, whenever a table is free.
3. As a receptionist, I should be able to check a customer's waiting number in the waiting list. (Example: "You are 3rd in the queue")

## Implementation
This app has two view controllers:

- __AdminViewController__: - This view controller provides two core features Book A Table, Customer Status

- __BookingViewController__: - This view controller provides implementation of Booking a table to costumer.If vacant tables are not available he is added to waiting queue.

- __BookingViewController__: - This view controller provides implementation of customer status i.e. positon status if he is in waiting queue.


- Application uses UIKit,Firebase.